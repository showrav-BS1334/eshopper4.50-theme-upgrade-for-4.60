﻿using FluentMigrator;
using Nop.Core.Domain.Catalog;

namespace Nop.Data.Migrations
{
    [NopMigration("2023/06/21 03:19:00:2551770", "Category. Add some new property", UpdateMigrationType.Data, MigrationProcessType.Update)]
    public class AddMinQuantityToProduct : AutoReversingMigration
    {
        public override void Up()
        {
            Create.Column(nameof(Product.MinQuantity))
            .OnTable(nameof(Product))
            .AsInt32()
            .Nullable();
        }
    }
}